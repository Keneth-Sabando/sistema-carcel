DROP TABLE IF EXISTS celda;

DROP TABLE IF EXISTS fichaReo;

DROP TABLE IF EXISTS pabellonUsuario;

DROP TABLE IF EXISTS pabellon;

DROP TABLE IF EXISTS usuario;

create table usuario(
    id_usuario serial,
    nombre varchar(255),
    correo varchar(100) unique,
    clave varchar(100),
    estado varchar(1),
    roles json,
    constraint pk_usuario primary key (id_usuario)
);

create table pabellon(
    id_pabellon serial,
    nombre varchar(100),
    estado varchar(1),
    constraint pk_pabellon primary key (id_pabellon)
);


create table pabellonUsuario(
    id_pa_us serial,
    id_pabellon int,
    id_usuario int,
    constraint pk_pabellonUsuario primary key (id_pa_us),
    constraint fk_pabellon foreign key (id_pabellon) references pabellon (id_pabellon),
    constraint fk_usuario foreign key (id_usuario) references usuario (id_usuario)
);


create table fichaReo(
    id_ficha serial,
    validad varchar(1),
    delitos varchar(255),
    sentencia varchar(255),
    informacion varchar(255),
    estado varchar(1),
    constraint pk_ficha primary key (id_ficha)
);


create table celda(
    id_celda serial,
    capacitadad int,
    numero int,
    estado varchar(1),
    id_pabellon int,
    id_ficha int,
    constraint pk_celda primary key (id_celda),
    constraint fk_pabellon foreign key (id_pabellon) references pabellon (id_pabellon),
    constraint fk_ficha foreign key (id_ficha) references fichaReo (id_ficha)
);


