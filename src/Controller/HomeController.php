<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;

class HomeController extends AbstractController
{

    public function __construct(private Security $security)
    {
    }

    #[Route(path: '/', name: 'app_home')]
    public function home(): Response
    {
        return $this->render('/home/home.html.twig');
    }
}
