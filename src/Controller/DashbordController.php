<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\SecurityBundle\Security;

#[Route('/dashbord')]
class DashbordController extends AbstractController
{

    public function __construct(private Security $security)
    {
    }

    #[Route('/public', name: 'app_dashbord')]
    public function public(): Response
    {
        return $this->render('dashbord/dashbord.html.twig');
    }
}
